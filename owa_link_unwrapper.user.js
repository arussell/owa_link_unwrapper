// ==UserScript==
// @copyright 2011+, Adam Russell
// @license X11 License; http://www.xfree86.org/3.3.6/COPYRIGHT2.html#3
// @name OWA Link Unwrapper
// @description Removes the redirection wrapper
//              around links in Outlook Web Access.
// @include https://*/owa/*
// @match https://*/owa/*
// ==/UserScript==


/**
 *  URL encode / decode
 *  http://www.webtoolkit.info/
 */
var Url = {
  // public method for url encoding
  encode : function (string) {
    return escape(this._utf8_encode(string));
  },

  // public method for url decoding
  decode : function (string) {
    return this._utf8_decode(unescape(string));
  },

  // private method for UTF-8 encoding
  _utf8_encode : function (string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);

      if (c < 128) {
        utftext += String.fromCharCode(c);
      } else if((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      } else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }
    }
    return utftext;
  },

  // private method for UTF-8 decoding
  _utf8_decode : function (utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while ( i < utftext.length ) {

      c = utftext.charCodeAt(i);

      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      }
      else if((c > 191) && (c < 224)) {
        c2 = utftext.charCodeAt(i+1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      }
      else {
        c2 = utftext.charCodeAt(i+1);
        c3 = utftext.charCodeAt(i+2);
        string += String.fromCharCode(
          ((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }
    }
    return string;
  }
}


// unwrap a redirecting link
function unwrapLink(link) {
  var href;
  var urlStartPos;
  var urlEndPos;

  href = link.getAttribute("href");

  // extract the actual URL, which will be encoded
  urlStartPos = href.indexOf("&URL=");
  if (urlStartPos == -1) {
    urlStartPos = href.indexOf("?URL=");
  }
  if (urlStartPos == -1) {
    return;
  }
  urlStartPos += 5;
  urlEndPos = href.indexOf("&", urlStartPos);
  if (urlEndPos == -1) {
    href = href.substring(urlStartPos);
  } else {
    href = href.substring(urlStartPos, urlEndPos);
  }

  // decode the URL
  href = Url.decode(href);

  // replace the original, wrapped link with the unwrapped one
  link.setAttribute("href", href);
}


// find wrapped, redirecting links inside messages
function unwrapMessageLinks() {
  var links;

  if (window.length < 1) {
    return;
  }

  links = window.frames[0].document.evaluate(
    "//a[starts-with(@href, 'redir.aspx')]",
    window.frames[0].document,
    null,
    XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
    null);

  for (var i = 0; i < links.snapshotLength; ++i) {
    unwrapLink(links.snapshotItem(i));
  }
}


// find wrapped, redirecting links inside messages (OWA Light)
function unwrapLightMessageLinks() {
  var links;

  links = document.evaluate(
    "//div[@class='bdy']//a[starts-with(@href, 'redir.aspx')]",
    document,
    null,
    XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
    null);

  for (var i = 0; i < links.snapshotLength; ++i) {
    unwrapLink(links.snapshotItem(i));
  }
}


// find wrapped, redirecting links inside reading pane messages
function unwrapReadingPaneMessageLinks() {
  var links;

  links = document.evaluate(
    "//div[@id='divBdy']//a[starts-with(@href, 'redir.aspx')]",
    document,
    null,
    XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
    null);

  for (var i = 0; i < links.snapshotLength; ++i) {
    unwrapLink(links.snapshotItem(i));
  }
}


// hook into the reading pane
function setUpReadingPane() {
  var divs = document.evaluate(
    "//div[@id='divRP']",
    document,
    null,
    XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
    null);

  for (var i = 0; i < divs.snapshotLength; ++i) {
    var div = divs.snapshotItem(i);
    div.addEventListener("DOMNodeInserted", unwrapReadingPaneMessageLinks, false);
  }
}


// main
unwrapLightMessageLinks();
setUpReadingPane();
document.addEventListener("DOMNodeInserted", unwrapMessageLinks, false);
